<?php

//######################################
//######################################-- SQDB
//######################################-- Simple Query DataBase
//######################################-- 
//######################################-- The database handler for simple queries and multi database support at the same time.
//######################################-- This was created to have a simple function calls and stats recording for debugging.
//######################################-- If you have any ideas feel free to message me.
//######################################-- 
//######################################-- Created by Anthony Lee @anthony on Hey.Café
//######################################-- https://gitlab.com/justanthonylee/sqdb
//######################################

//--Main Variables
$sqdb=array();
$sqdb["stats"]=array();
$sqdb["settings"]=array();
$sqdb["database"]=array();
$sqdb_connections=array();
$sqdb_queries=array();

//--Stats recording values
$sqdb["stats"]["connect"]=0;
$sqdb["stats"]["query"]=0;
$sqdb["stats"]["num"]=0;
$sqdb["stats"]["fetch"]=0;
$sqdb["stats"]["insert"]=0;
$sqdb["stats"]["time_connect"]=0;
$sqdb["stats"]["time_query"]=0;
$sqdb["stats"]["time_num"]=0;
$sqdb["stats"]["time_fetch"]=0;
$sqdb["stats"]["time_insert"]=0;

//--Settings
$sqdb["settings"]["error_crash"]=true;
$sqdb["settings"]["error_function"]=false;

//################################
//################################-- Database Connections
//################################-- Default is the name used when one is not provided in the query call
//################################-- You can also not add any here and use sqdb_create_connect() to add in your code before use
//################################

//sqdb_create_connect("default","localhost","username","password","database");

//################################
//################################-- SQBD -> Create -> Connect
//################################

function sqdb_create_connect($connection,$server,$username,$password,$database){
	global $sqdb_connections;
	global $sqdb;
	
	if (!isset($sqdb["database"]["".$connection.""])){
		$sqdb["database"]["".$connection.""]=array();
	}
	
	$sqdb["database"]["".$connection.""]["db_server"]=$server;
	$sqdb["database"]["".$connection.""]["db_username"]=$username;
	$sqdb["database"]["".$connection.""]["db_password"]=$password;
	$sqdb["database"]["".$connection.""]["db_database"]=$database;
	
	return true;
}

//################################
//################################-- SQBD -> Settings -> Update
//################################

function sqdb_settings_update($name,$value){
	global $sqdb;
	
	$sqdb["settings"]["".$name.""]=$value;
	
	return true;
}

//################################
//################################-- SQBD -> Error -> Crash
//################################

function sqdb_error_crash($message){
	global $sqdb;
	
	if ($sqdb["settings"]["error_function"]!=false){
		call_user_func($sqdb["settings"]["error_function"], $message);
	}
	
	if ($sqdb["settings"]["error_crash"]==true){
		die("#SQDB Crash Error: ".$message."");
	}
}

//################################
//################################-- SQBD -> Connect -> Database
//################################

function sqdb_connect_database($connection){
	global $sqdb_connections;
	global $sqdb;
	
	if (!isset($sqdb_connections["".$connection.""])){
		$sqdb["stats"]["connect"]=$sqdb["stats"]["connect"]+1;
		$time_start=microtime(true);
		
		$sqdb_connections["".$connection.""]=mysqli_init();
		if (!$sqdb_connections["".$connection.""]){
			sqdb_error_crash('Init failed');
		}
		
		if (!$sqdb_connections["".$connection.""]->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5)){
			sqdb_error_crash('Setting OPT_CONNECT_TIMEOUT failed');
		}
		
		if (!$sqdb_connections["".$connection.""]->real_connect($sqdb["database"]["".$connection.""]["db_server"], $sqdb["database"]["".$connection.""]["db_username"], $sqdb["database"]["".$connection.""]["db_password"], $sqdb["database"]["".$connection.""]["db_database"])){
			sqdb_error_crash("We are unable to conect to our backend systems. Try again in a few minutes, we may be under heavy load.");
		}
		
		$time_end=microtime(true);
		$sqdb["stats"]["time_connect"]=$sqdb["stats"]["time_connect"]+round(($time_end-$time_start)*1000);
	}
}

//################################
//################################-- SQBD -> Query
//################################

function sqdb_query($query,$connection="default"){
	global $sqdb_connections;
	global $sqdb;
	global $sqdb_queries;
	$return=null;
	
	//--Check if connection has been made
	if (!isset($sqdb_connections["".$connection.""])){ sqdb_connect_database($connection); }
	$return=false;

	//--Start stats recording
	$sqdb["stats"]["query"]=$sqdb["stats"]["query"]+1;
	$time_start=microtime(true);
	
	//--Do work
	$return = $sqdb_connections["".$connection.""]->query($query);
	
	//--Stop stats recording
	$time_end=microtime(true);
	$sqdb["stats"]["time_query"]=$sqdb["stats"]["time_query"]+round(($time_end-$time_start)*1000);
	
	//--Record advanced query stats
	$stats_q=array();
	$stats_q["time"]="".round((($time_end-$time_start)*1000),5)."ms";
	$stats_q["query"]=$query;
	array_push($sqdb_queries, $stats_q);
	
	//--Return data
	return $return;
}

//################################
//################################-- SQBD -> Num -> Rows
//################################

function sqdb_num_rows($query,$connection="default"){
	global $sqdb_connections;
	global $sqdb;
	$return=null;
	
	//--Check if connection has been made
	if (!isset($sqdb_connections["".$connection.""])){ sqdb_connect_database($connection); }
	
	//--Start stats recording
	$sqdb["stats"]["num"]=$sqdb["stats"]["num"]+1;
	$time_start=microtime(true);
	
	//--Do work
	if ($query !== false){
		$return=$query->num_rows;
	}else{
		$return=0;
	}
	
	//--Stop stats recording
	$time_end=microtime(true);
	$sqdb["stats"]["time_num"]=$sqdb["stats"]["time_num"]+round(($time_end-$time_start)*1000);
	
	//--Return data
	return $return;
}

//################################
//################################-- SQBD -> Fetch -> Array
//################################

function sqdb_fetch_array($query,$connection="default"){
	global $sqdb_connections;
	global $sqdb;
	$return=null;
	
	//--Check if connection has been made
	if (!isset($sqdb_connections["".$connection.""])){ sqdb_connect_database($connection); }
	
	//--Start stats recording
	$sqdb["stats"]["fetch"]=$sqdb["stats"]["fetch"]+1;
	$time_start=microtime(true);
	
	//--Do work
	$return=$query->fetch_assoc();
	
	//--Stop stats recording
	$time_end=microtime(true);
	$sqdb["stats"]["time_fetch"]=$sqdb["stats"]["time_fetch"]+round(($time_end-$time_start)*1000);
	
	//--Return data
	return $return;
}

//################################
//################################-- SQBD -> Insert -> ID
//################################

function sqdb_insert_id($connection="default"){
	global $sqdb_connections;
	global $sqdb;
	$return=null;
	
	//--Check if connection has been made
	if (!isset($sqdb_connections["".$connection.""])){ sqdb_connect_database($connection); }
	
	//--Start stats recording
	$sqdb["stats"]["insert"]=$sqdb["stats"]["insert"]+1;
	$time_start=microtime(true);
	
	//--Do work
	$return=$sqdb_connections["".$connection.""]->insert_id;
	
	//--Stop stats recording
	$time_end=microtime(true);
	$sqdb["stats"]["time_insert"]=$sqdb["stats"]["time_insert"]+round(($time_end-$time_start)*1000);
	
	//--Return data
	return $return;
}

//################################
//################################-- SQBD -> Close
//################################

function sqdb_close($connection="default"){
	global $sqdb_connections;
	global $sqdb;
	
	//--Check if connection has been made
	if (!isset($sqdb_connections["".$connection.""])){ sqdb_connect_database($connection); }
	
	//--Close
	$sqdb_connections["".$connection.""]->close();
	
	return true;
}

//################################
//################################-- SQBD -> Report -> Queries
//################################

function sqdb_report_queries(){
	global $sqdb_queries;
	return json_encode($sqdb_queries, JSON_PRETTY_PRINT);
}

//################################
//################################-- SQBD -> Report -> Stats
//################################

function sqdb_report_stats(){
	global $sqdb;
	return json_encode($sqdb["stats"], JSON_PRETTY_PRINT);
}