# SQDB

This project allows you to use simple database calls that can be a drop in replacement for mysql() in PHP. This was first made a decade ago and then was slowly used in diffrent projects. It does not do any cleaning to your queries before they run so you should be doing that before you send your query to the functions.

You can add your database details directly or add them by after including the project file into your code just call ($connection,$server,$username,$password,$database) with connection being the NAME you can call in a query at the end to switch what database you are using, the default is "default" so use this if you dont call a connection name at the end of your function call.